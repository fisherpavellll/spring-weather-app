package com.fisherpavel.springweatherapp.service;

import com.fisherpavel.springweatherapp.dto.CityDto;
import com.fisherpavel.springweatherapp.entity.City;
import com.fisherpavel.springweatherapp.mapper.CityMapper;
import com.fisherpavel.springweatherapp.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CityServiceImpl implements CityService {
    private CityRepository cityRepository;
    private CityMapper cityMapper = CityMapper.MAPPER;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository){
        this.cityRepository = cityRepository;
    }

    public List<CityDto> getAllCities(){
        return cityRepository.findByDeleted(false)
                .stream()
                .map(cityMapper::fromCity)
                .collect(Collectors.toList());
    }

    public Map<String, Object> getAllCitiesPage(String title, int page, int size, String sortBy){
        List<City> cities = new ArrayList<>();
        Pageable paging = PageRequest.of(page, size, Sort.by(sortBy));

        Page<City> pageCities;

        if(title == null){
            pageCities = cityRepository.findByDeleted(false, paging);
        } else {
            pageCities = cityRepository.findByNameContainsAndDeleted(title, paging, false);
        }

        cities = pageCities.getContent();

        Map<String, Object> response = new HashMap<>();
        response.put("cities", cities);
        response.put("currentPage", pageCities.getNumber());
        response.put("totalItems", pageCities.getTotalElements());
        response.put("totalPages", pageCities.getTotalPages());

        return response;
    }

    public CityDto getCityById(int id) {

        Optional<City> optional = cityRepository.findByIdAndDeleted(id, false);

        optional.orElseThrow(() -> new EntityNotFoundException("There is no City with id: " + id));

        return cityMapper.fromCity(optional.get());
    }

    public void saveCity(City city){
        cityRepository.save(city);
    }

    public void deleteCity(int id) {
        Optional<City> optional = cityRepository.findByIdAndDeleted(id, false);

        optional.orElseThrow(() -> new EntityNotFoundException("There is no City with id: " + id));

        City city = optional.get();

//        city.isDeleted();

        cityRepository.save(city);
    }
}
