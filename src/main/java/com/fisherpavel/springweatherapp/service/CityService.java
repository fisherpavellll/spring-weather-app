package com.fisherpavel.springweatherapp.service;

import com.fisherpavel.springweatherapp.dto.CityDto;
import com.fisherpavel.springweatherapp.entity.City;

import java.util.List;
import java.util.Map;

public interface CityService {
     List<CityDto> getAllCities();
     void saveCity(City city);
     CityDto getCityById(int id);
     void deleteCity(int id);
     Map<String, Object> getAllCitiesPage(String title, int page, int size, String sortBy);
}
