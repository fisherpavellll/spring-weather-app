package com.fisherpavel.springweatherapp.mapper;

import com.fisherpavel.springweatherapp.dto.WeatherDto;
import com.fisherpavel.springweatherapp.entity.Weather;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WeatherMapper {
    WeatherMapper MAPPER = Mappers.getMapper(WeatherMapper.class);

    Weather toWeather(WeatherDto weatherDto);

    @InheritInverseConfiguration
    WeatherDto fromWeather(Weather weather);
}
