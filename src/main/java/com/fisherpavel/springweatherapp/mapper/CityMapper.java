package com.fisherpavel.springweatherapp.mapper;

import com.fisherpavel.springweatherapp.dto.CityDto;
import com.fisherpavel.springweatherapp.entity.City;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {WeatherMapper.class})
public interface CityMapper {

  CityMapper MAPPER = Mappers.getMapper(CityMapper.class);

  City toCity(CityDto cityDto);

  @InheritInverseConfiguration
  CityDto fromCity(City city);
}
