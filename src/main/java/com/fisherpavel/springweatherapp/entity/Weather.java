package com.fisherpavel.springweatherapp.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "weather")
public class Weather {

    @Id
    @Column(name = "id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "temperature")
    private String temperature;

    @Column(name = "windspeed")
    private String windSpeed;

    @Column(name = "winddirection")
    private String windDirection;

    @Column(name = "city_id")
    private int cityId;

}
