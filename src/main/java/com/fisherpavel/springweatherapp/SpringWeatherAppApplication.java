package com.fisherpavel.springweatherapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringWeatherAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWeatherAppApplication.class, args);
    }
}
