package com.fisherpavel.springweatherapp.repository;

import com.fisherpavel.springweatherapp.dto.CityDto;
import com.fisherpavel.springweatherapp.entity.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Integer> {
    Page<City> findByNameContainsAndDeleted(String name, Pageable pageable, boolean deleted);
    Page<City> findByDeleted(boolean deleted, Pageable pageable);
    List<City> findByDeleted(boolean deleted);
    Optional<City> findByIdAndDeleted(int id, boolean deleted);
}
