package com.fisherpavel.springweatherapp.dto;

import com.fisherpavel.springweatherapp.entity.Weather;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


public class CityDto {
    private int id;
    private int name;
    private boolean deleted;
    private List<WeatherDto> weathers;
}
