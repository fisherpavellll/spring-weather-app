package com.fisherpavel.springweatherapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class WeatherDto {
    private int id;
    private String temperature;
    private String windSpeed;
    private String windDirection;
    private int cityId;
}
